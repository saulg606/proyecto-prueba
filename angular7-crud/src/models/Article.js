let mysqlConfig = require("../Utilities/mysqlConfig");

let initialize = () => {
mysqlConfig.getDB().query("create table IF NOT EXISTS location (id INT auto_increment primary key, name VARCHAR(30), area_m2 numeric)");

}

module.exports = {
initialize: initialize
}